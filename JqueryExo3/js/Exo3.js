/*
    Permet d'éditer la DIV
    Site Web
    Julien LESSART
    Version 1.0
    16/03/20
*/

$(document).ready(function() {
    //Code à exécuter après le chargement du DOM
    //Execution de la fonction anonyme lors du clic sur le bouton 

    $("#btn1").click(function() {
        // Quand on clique sur l 'id btn1
        $("#maDiv1").html("Coucou du Jquery grace a ce bouton"); // On edit la DIV 
    });
});