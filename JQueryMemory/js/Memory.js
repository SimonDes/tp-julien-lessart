/*
    Jeu du Memory en jQuery
    Site Web
    Julien LESSART
    Version 1.0
    16/03/20
*/


// Déclarations des variables et constante
const NBCASES = 16; // Pour le nombre de cases
let arrayCartes = []; // Tableau contenant les cartes (var car variable utilisé plus haut dans le programme)
let cartesRetournees = []; // Tableau contenant les cartes retourner (var car réutiliser plus haut)
let indCarteCliquee = ""; // Pour récuperer le id de la carte cliquée (var car réutiliser dans aff())
let valImg = -1; // Pour changer l'affichage de l'image
let verifPaires = 0; // init à 0 car c'est le compteur de paires
let verifClique = true; // Pour savoir si on a le droit de cliquer


$(document).ready(function() { // Pour charger le Jquery au chargement de la page
    creerPlateauJeu();

    $("#conteneurJeu").on("click", "div", function() {
        // Quand on clique sur une carte
        // Aucun prm
        // Aucun valeur de retour

        // Pour ne pas pouvoir cliquer sur plus de deux cartes

        verifClique = false;
        if (cartesRetournees.length < 2) {
            // On récupère le numéro de la carte sur laquelle
            // on a cliqué à partir de son attribut data-id
            indCarteCliquee = $(this).data("id"); // Pour récuperer le id de la carte cliquée (var car réutiliser dans aff())
            $(this).removeClass("carteDos"); // Pour supprimer la clase carteDos

            // Pour ne pas pouvoir cliquer sur une carte déjà trouvée
            if ((arrayCartes[indCarteCliquee].trouvee) !== -1) {
                $(this).addClass(arrayCartes[indCarteCliquee].img); // On ajoute la classe arrayCartes
            }

            // Si l'attribut trouvee de la carte est égàl à 0
            if (arrayCartes[indCarteCliquee].trouvee == 0) {
                arrayCartes[indCarteCliquee].trouvee = 1; // Il sera égal à 1
                cartesRetournees.push(indCarteCliquee); // Et on l'ajoute a carteRetournees
                //aff(indCarteCliquee); // Et on affiche la carte avec la fonction aff(prmCartes)
                jeu(); // On utilise la fonction jeu()
            }
        }
    });

});

function creerPlateauJeu() {
    // On cree le plateau de jeu
    // Aucun prm
    // Aucune valeur de retour

    for (let i = 0; i < NBCASES; i++) {
        $("#conteneurJeu").append('<div class="carteDos"data-id = "' + i + '" ></div>'); // Pour ajouter le nombre de carte
    }

    // Pour stocker les informations des cases
    for (let i = 0; i < 2; i++) {
        arrayCartes.push({ img: "img0", trouvee: 0 });
        arrayCartes.push({ img: "img1", trouvee: 0 });
        arrayCartes.push({ img: "img2", trouvee: 0 });
        arrayCartes.push({ img: "img3", trouvee: 0 });
        arrayCartes.push({ img: "img4", trouvee: 0 });
        arrayCartes.push({ img: "img5", trouvee: 0 });
        arrayCartes.push({ img: "img6", trouvee: 0 });
        arrayCartes.push({ img: "img7", trouvee: 0 });
    }

    // Pour changer la position des cartes
    for (let position = arrayCartes.length - 1; position >= 1; position--) {
        let hasard = Math.floor(Math.random() * (position + 1)); // Pour init l'aléatoire
        let sauve = arrayCartes[position]; // pour sauvegarde la position
        arrayCartes[position] = arrayCartes[hasard]; // pour l'échanger 
        arrayCartes[hasard] = sauve; // Pour la rechanger
    }
}

function rejouer() {
    // Pour rejouer
    // Aucun prm
    // Aucune valeur de retour

    location.reload(); // On reload la page
}

function aff(prmCartes) {
    // Pour gérer l'affichage d'une carte
    // un prm : prmCartes, la carte selectionner
    // Aucune valeur de retour

    // Pour ne pas pouvoir retourner les cartes déjà trouvée
    switch (arrayCartes[prmCartes].trouvee) {
        case 0: // Si 0
            $("." + valImg).removeClass(arrayCartes[prmCartes].img).addClass("carteDos"); // On remove la classe arrayCartes
            break;

        case 1: // Si on clique sur une image
            arrayCartes[prmCartes].src = arrayCartes[prmCartes].img; // On affiche l'image
            break;

        case -1: // Si il y a une paire
            $("." + valImg).removeClass(arrayCartes[prmCartes].img).addClass("carteTrouvee"); // On remove la classe arrayCartes
            break;
    }
}

function jeu() {
    // Permet de savoir si une paire à était trouvée et changer la valeur de l'array
    // Aucun prm
    // Aucune valeur de retour

    // Si on retourne moins de deux cartes
    if (cartesRetournees.length <= 2) {

        // Et que cette carte n'a pas était trouvée
        if (arrayCartes[indCarteCliquee].trouvee == 0) {
            arrayCartes[indCarteCliquee].trouvee = 1; // l'attribut trouvee = 1
            cartesRetournees.push(indCarteCliquee); // On l'ajoute au tableau des cartes retournees
            aff(indCarteCliquee); // On affiche la carte
        }

        // Si on a retourner deux cartes
        if (cartesRetournees.length == 2) {
            let nouveauEtat = 0; // Pour garder l'état de la carte

            // Si le tableau principal d'index 0 est égal au tableau principal d'index 1
            if (arrayCartes[cartesRetournees[0]].img == arrayCartes[cartesRetournees[1]].img) {
                nouveauEtat = -1; // l'état = -1
                verifPaires++; // On ajoute 1
            }

            // On ajoute la valeur de nouveauEtat à l'attribut trouvee
            // Si on a trouvé une paire nouveauEtat = -1
            // Sinon nouveauEtat = 0
            arrayCartes[cartesRetournees[0]].trouvee = nouveauEtat;
            arrayCartes[cartesRetournees[1]].trouvee = nouveauEtat;

            // Pour la durée du temps
            setTimeout(function() {
                valImg = arrayCartes[cartesRetournees[0]].img;
                aff(cartesRetournees[0]); // On désaffiche la carte d'index 0 ou on la retourne
                valImg = arrayCartes[cartesRetournees[1]].img;
                aff(cartesRetournees[1]); // On désaffiche la carte d'index 1 ou on la retourne

                cartesRetournees = []; // Ensuite on clear le tableau car on a cliqué sur deux cartes différentes
                if (verifPaires == 8) { // Si toute les paires ont était trouvées
                    rejouer(); // On lance la fonction rejouer()
                }
            }, 750);
        }
    }
}