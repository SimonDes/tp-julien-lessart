/*
    Test requete Ajax
    Site Web
    Julien LESSART
    Version 1.0
    03/05/20
*/

//Déclarations des variables
let valCin = ""; // Valeur de idNom
let urlRequete = ""; // Url de requete

$(document).ready(function() { // Pour charger le Jquery au chargement de la page

    $("#btnChercher").on("click", function() { // Quand on clique sur btnChercher

        valCin = $("#idNom").val(); // On recupere le texte de valCin
        urlRequete = "http://ou.comarquage.fr/api/v1/autocomplete-territory?kind=CommuneOfFrance&term=" + valCin.toString(); // On prend l'url de comarquage.fr puis on rajoute la string de valCin

        // Requete ajax
        $.ajax({
            type: "GET",
            url: urlRequete,
            dataType: "jsonp",
            jsonp: "jsonp",
            success: onGetCommuneSuccess,
            error: onGetCommuneError
        });
    });
});

// Si succes 
function onGetCommuneSuccess(reponse, status) {

    $("#divResult p").remove(); // Pour supprimier la saisie d'avant

    // On boucle tant que i est plus petit au égal au nombre d'items
    for (let i = 0; i <= reponse.data.currentItemCount; i++) {
        $("#divResult").append("<p>" + reponse.data.items[i].main_postal_distribution + "</p>"); // on ajoute a la fin de la divResult la réponse avec l'item i pour chaque main_postal_distribution
    }
}

// Si échec
function onGetCommuneError(status) {
    alert(JSON.stringify(status));
}