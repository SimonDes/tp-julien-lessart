/*
    Test requete Ajax
    Site Web
    Julien LESSART
    Version 1.0
    03/05/20
*/

//Déclarations des variables
let codeBarre = "", // Code barre du produit 
    urlRequete = ""; // Url de requete

$(document).ready(function() { // Pour charger le Jquery au chargement de la page

    $("#btnChercher").on("click", function() { // Quand on clique sur btnChercher

        Reset();
        codeBarre = $("#idCodeBarre").val(); // On recupere le texte de valCin
        urlRequete = "http://fr.openfoodfacts.org/api/v0/produit/" + codeBarre.toString() + ".json"; // On prend l'url de openfoodfacts puis on rajoute la string de codeBarre


        // Requete ajax
        $.ajax({
            type: "GET",
            url: urlRequete,
            success: onGetCommuneSuccess,
            error: onGetCommuneError
        });

    });
});

// Si succes 
function onGetCommuneSuccess(reponse, status) {

    // IHM de base
    $("#divResult").append("<p id=" + "idNom>" + reponse.product.generic_name + "</p>");
    $("#divResult").append("<p id=" + "idMarque>" + reponse.product.brands + "</p>");
    $("#divResult").append("<p> <img id=" + "idPhoto src=" + "" + "/> </p>");
    $("#idPhoto").attr("src", reponse.product.image_url); // On recupere le texte de nomProduit




    // Pour les nutriments pour 100g
    $("#tableNutriments").append(" <thead> <tr> <th> Composition nutritionnelle </th> <th> pour 100g / 100 ml</th> <th> par portion </th> </tr> </thead>");
    $("#tableNutriments").append("<tbody> <tr> <td> Energie </td> <td>" + reponse.product.nutriments.energy_100g + " kj</td> <td>" + reponse.product.nutriments.energy_value + " kj</td> </tr>");
    $("#tableNutriments").append("<tr> <td> Proteines </td> <td>" + reponse.product.nutriments.proteins_100g + " g</td> <td>" + reponse.product.nutriments.proteins_value + " g</td> </tr>");
    $("#tableNutriments").append("<tr> <td> Glucides </td> <td>" + reponse.product.nutriments.carbohydrates_100g + " g</td> <td>" + reponse.product.nutriments.carbohydrates_value + " g</td> </tr>");
    $("#tableNutriments").append("<tr> <td> dont Sucres </td> <td>" + reponse.product.nutriments.sugars_100g + " g</td> <td>" + reponse.product.nutriments.sugars_value + " g</td> </tr>");
    $("#tableNutriments").append("<tr> <td> Lipides </td> <td>" + reponse.product.nutriments.fat_100g + " g</td> <td>" + reponse.product.nutriments.fat_value + " g</td> </tr>");
    $("#tableNutriments").append("<tr> <td> dont Acides gras </td> <td>" + reponse.product.nutriments.fat_100g + " g</td> <td>" + reponse.product.nutriments.fat + " g</td> </tr>");
    $("#tableNutriments").append("<tr> <td> Fibres alimentaires </td> <td>" + reponse.product.nutriments.fiber_100g + " g</td> <td>" + reponse.product.nutriments.fiber_value + " g</td> </tr>");
    $("#tableNutriments").append("<tr> <td> Sodium </td> <td>" + reponse.product.nutriments.sodium_100g + " g</td> <td>" + reponse.product.nutriments.sodium + " g</td> </tr>");
    $("#tableNutriments").append("<tr> <td> Équivalent Sel </td> <td>" + reponse.product.nutriments.salt_100g + " g</td> <td>" + reponse.product.nutriments.salt_value + " g</td> </tr> </tbody>");

    // Tableau des ingredients
    for (let i = 0; i < reponse.product.ingredients_n; i++) {
        $("#divIngredients").append("<p>" + reponse.product.ingredients[i].text + "</p>");
    }
}

// Si échec
function onGetCommuneError(status) {
    alert(JSON.stringify(status));
}

function Reset() {
    // Permet de reset le produit
    // Aucun prm
    // Aucune valeur de retour

    $("#idNom").remove();
    $("#idMarque").remove();
    $("#idPhoto").remove();
    $("#tableNutriments thead").remove();
    $("#tableNutriments tbody").remove();
    $("#divIngredients p").remove();
}